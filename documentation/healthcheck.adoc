include::{lang}/variables.txt[]
= {subject} Prepared for {customer}
Red Hat Cloud Success Team
:toc:
ifdef::backend-pdf[]
:pygments-style: tango
:source-highlighter: pygments
:title-page-background-image: image:fondo.png[pdfwidth=50%,position=bottom right]

endif::[]


include::{lang}/chapters/versions.adoc[]

include::{lang}/chapters/preface.adoc[]

include::{lang}/chapters/architecture.adoc[]

include::{lang}/chapters/appendix/appendix_checklist.adoc[]

include::{lang}/chapters/appendix/appendix_data.adoc[]
